# js-games

(c) 2021-2022 iradu, ivictor
license: BSD

## Javascript Games:

see other copyrights in the specific game folder license files

### Js Chess
license: GPL

### Js Mahjong
license: GPLv3

### Js Monopoly
license: BSD

### Js Poker
license: BSD

### Js Poker TH (Texas Hold'em)
license: GPLv2

### Js Roulette
license: GPLv3

### Js Slot Machine
license: MIT

